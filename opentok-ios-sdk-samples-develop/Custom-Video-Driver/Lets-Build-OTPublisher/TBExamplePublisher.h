//
//  TBPublisher.h
//  Lets-Build-OTPublisher
//
//  Copyright (c) 2013 TokBox, Inc. All rights reserved.
//

#import <OpenTok/OpenTok.h>
#import <GLKit/GLKit.h>

@interface TBExamplePublisher : OTPublisherKit

@property(readonly) GLKView* view;

@property(nonatomic, assign) AVCaptureDevicePosition cameraPosition;
- (id)initWithDelegate:(id<OTPublisherDelegate>)delegate name:(NSString*)name publisherView:(UIView *)pView;

@end
