//
//  TBExampleVideoCapture.h
//  OpenTok iOS SDK
//
//  Copyright (c) 2013 Tokbox, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <OpenTok/OpenTok.h>
#import <GLKit/GLKit.h>
#import <CoreMedia/CoreMedia.h>
#import "GSGreenScreenEffect.h"

@protocol OTVideoCapture;

@interface TBExampleVideoCapture : NSObject
    <AVCaptureVideoDataOutputSampleBufferDelegate, OTVideoCapture>
{
    @protected
    dispatch_queue_t _capture_queue;
    BOOL readyToRecordVideo;
    AVAssetWriter *assetWriter;
    CMBufferQueueRef previewBufferQueue;
    NSMutableArray *previousSecondTimestamps;
    AVCaptureVideoOrientation referenceOrientation;
}

@property (nonatomic, readwrite, assign)
CVOpenGLESTextureCacheRef videoTextureCache;
@property (readwrite) Float64 videoFrameRate;
@property (readwrite) CMVideoCodecType videoType;
@property (strong, nonatomic) GLKBaseEffect *baseEffect;
@property (strong, nonatomic) GSGreenScreenEffect *greenScreenEffect;
@property (strong, nonatomic) GLKTextureInfo *background;


@property (nonatomic, retain) AVCaptureSession *captureSession;
@property (nonatomic, retain) AVCaptureVideoDataOutput *videoOutput;
@property (nonatomic, retain) AVCaptureDeviceInput *videoInput;

@property (nonatomic, assign) NSString* captureSessionPreset;
@property (readonly) NSArray* availableCaptureSessionPresets;

@property (nonatomic, assign) double activeFrameRate;
- (BOOL)isAvailableActiveFrameRate:(double)frameRate;

@property (nonatomic, assign) AVCaptureDevicePosition cameraPosition;
@property (readonly) NSArray* availableCameraPositions;
@property (nonatomic, strong) UIView *pView;
@property (readwrite) CMVideoDimensions videoDimensions;

- (BOOL)toggleCameraPosition;
- (void)makeViewReady:(UIView *)pView;
@end
